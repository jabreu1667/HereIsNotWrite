class Task < ActiveRecord::Base
  has_many :person_tasks
  has_many :people, through: :person_tasks
  belongs_to :topic
  validates :title, :topic_id, presence: true
  after_destroy :log_destroy_action

   before_create do
    self.date_end =((DateTime.now.to_date) + 7) if date_end.blank?
  end
 
  def log_destroy_action
    puts 'Eliminando las Tareas'
  end
  
end
