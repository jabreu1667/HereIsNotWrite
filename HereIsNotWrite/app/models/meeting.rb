class Meeting < ActiveRecord::Base
	has_many :people , through: :meeting_people
  has_many :meeting_people
  validates :motive, presence: true
  
  before_create do
    self.date = DateTime.now.to_date if date.blank?
    self.time = Time.now.strftime "%H:%M:%S" if time.blank?
    self.time = time.strftime "%H:%M:%S" unless time.blank?

  end


end
