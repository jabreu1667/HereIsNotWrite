class Person < ActiveRecord::Base
  has_many :meeting_people
  has_many :meetings , through: :meeting_people 
  has_many :person_tasks
  has_many :tasks, through: :person_tasks
  #Validations
  validates :name, presence: true, confirmation: true
  validates :email, presence: true, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }
  validates :email, uniqueness: true, on: :create
  
end
