class Agreement < ActiveRecord::Base
  belongs_to :topic
  validates :title, :topic_id, presence: true
  after_destroy :log_destroy_action
 
  def log_destroy_action
    puts 'Eliminaron los acuerdos'
  end
end
