class Topic < ActiveRecord::Base
  has_many :agreements , dependent: :destroy
  has_many :tasks, dependent: :destroy
  validates :title, presence: true
  before_create do
  self.duration = duration.strftime "%H:%M:%S" unless duration.blank?
  end
end
