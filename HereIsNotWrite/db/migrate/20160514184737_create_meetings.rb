class CreateMeetings < ActiveRecord::Migration
  def change
    create_table :meetings do |t|
      t.string :motive
      t.time :time
      t.date :date
      t.string :place

      t.timestamps null: false
    end
  end
end
