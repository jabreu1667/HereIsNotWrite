class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :title
      t.date :date_end
      t.string :status
      t.references :topic, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
