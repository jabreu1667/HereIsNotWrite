class CreateJoinTableMeetingTopic < ActiveRecord::Migration
  def change
    create_join_table :meetings, :topics do |t|
      # t.index [:meeting_id, :topic_id]
      # t.index [:topic_id, :meeting_id]
    end
  end
end
