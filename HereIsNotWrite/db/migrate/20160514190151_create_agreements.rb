class CreateAgreements < ActiveRecord::Migration
  def change
    create_table :agreements do |t|
      t.string :title
      t.references :topic, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
