# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160514201812) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "agreements", force: :cascade do |t|
    t.string   "title"
    t.integer  "topic_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "agreements", ["topic_id"], name: "index_agreements_on_topic_id", using: :btree

  create_table "meeting_people", force: :cascade do |t|
    t.integer  "meeting_id"
    t.integer  "person_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "meeting_people", ["meeting_id"], name: "index_meeting_people_on_meeting_id", using: :btree
  add_index "meeting_people", ["person_id"], name: "index_meeting_people_on_person_id", using: :btree

  create_table "meetings", force: :cascade do |t|
    t.string   "motive"
    t.time     "time"
    t.date     "date"
    t.string   "place"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "meetings_topics", id: false, force: :cascade do |t|
    t.integer "meeting_id", null: false
    t.integer "topic_id",   null: false
  end

  create_table "people", force: :cascade do |t|
    t.string   "name"
    t.string   "last_name"
    t.string   "email"
    t.string   "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "person_tasks", force: :cascade do |t|
    t.integer  "person_id"
    t.integer  "task_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "person_tasks", ["person_id"], name: "index_person_tasks_on_person_id", using: :btree
  add_index "person_tasks", ["task_id"], name: "index_person_tasks_on_task_id", using: :btree

  create_table "tasks", force: :cascade do |t|
    t.string   "title"
    t.date     "date_end"
    t.string   "status"
    t.integer  "topic_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "tasks", ["topic_id"], name: "index_tasks_on_topic_id", using: :btree

  create_table "topics", force: :cascade do |t|
    t.string   "title"
    t.time     "duration"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "agreements", "topics"
  add_foreign_key "meeting_people", "meetings"
  add_foreign_key "meeting_people", "people"
  add_foreign_key "person_tasks", "people"
  add_foreign_key "person_tasks", "tasks"
  add_foreign_key "tasks", "topics"
end
