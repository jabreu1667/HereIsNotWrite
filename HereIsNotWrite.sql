CREATE DATABASE hereisnotwrite()
﻿CREATE TABLE meetings( 
id serial NOT NULL PRIMARY KEY, motive character varying, hora time, fecha date, place character varying);
CREATE TABLE people(
id serial NOT NULL PRIMARY KEY, name character varying(20), last_name character varying(20), email character varying(20), 
phone character varying(15));
CREATE TABLE meeting_people(
id serial NOT NULL PRIMARY KEY, 
meeting_id integer REFERENCES meetings (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
people_id integer REFERENCES people (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE);
CREATE TABLE topics(
id serial NOT NULL PRIMARY KEY, title character varying, duration time);
CREATE TABLE topics_meeting(
id serial NOT NULL PRIMARY KEY, 
meeting_id integer REFERENCES meetings (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
topic_id integer REFERENCES topics (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE);
CREATE TABLE tasks(
id serial NOT NULL PRIMARY KEY, title character varying, date_end date, status character varying,
topic_id integer REFERENCES topics (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE );
CREATE TABLE people_tasks(
id serial NOT NULL PRIMARY KEY, 
task_id integer REFERENCES tasks (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
people_id integer REFERENCES people (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE);
CREATE TABLE agreements(
id serial NOT NULL PRIMARY KEY, title character varying,
topic_id integer REFERENCES topics (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE )



